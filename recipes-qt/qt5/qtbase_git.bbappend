# Prevent qtbase configure including gl.h
# fatal error: GL/gl.h: No such file or directory
#
# It seems, that the wrong configure options are selected
# So append them here.
#
# The cause:
# Raspberrypi layer only installs opengles2 headers into sysroot!
# So the most important configure option is "gles2"


PACKAGECONFIG_append_raspberrypi2 = " eglfs fontconfig gles2 linuxfb "
