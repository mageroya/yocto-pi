DESCRIPTION = "Package group for Qt5 demos"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} += " \
    qtbase-examples \
    qtdeclarative-examples \
    qt3d-examples \
    qtsmarthome \
    qt5ledscreen \
    quitbattery \
    qt5everywheredemo \ 
    qt5nmapcarousedemo \
    qt5nmapper \
    cinematicexperience \
    quitindicators \
    qt5-demo-extrafiles \
"
