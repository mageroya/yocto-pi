# Copyright © Benedikt Niedermayr 2016

inherit relocatable

DEPENDS_remove="${BASEDEPENDS} "

# Set target
TARGET_PREFIX="avr-"
TARGET_ARCH="avr"
MACHINE="avr"

STAGING_DIR = "${TMPDIR}/sysroots"
STAGING_DIR_NATIVE = "${STAGING_DIR}/${BUILD_SYS}"
STAGING_BINDIR_NATIVE = "${STAGING_DIR_NATIVE}${bindir_native}"
STAGING_BINDIR_CROSS = "${STAGING_BINDIR}/crossscripts"
STAGING_BINDIR_TOOLCHAIN = "${STAGING_DIR_NATIVE}${bindir_native}/${TARGET_ARCH}${TARGET_VENDOR}-${TARGET_OS}"
STAGING_LIBDIR_NATIVE = "${STAGING_DIR_NATIVE}${libdir_native}"
STAGING_LIBEXECDIR_NATIVE = "${STAGING_DIR_NATIVE}${libexecdir_native}"
STAGING_BASE_LIBDIR_NATIVE = "${STAGING_DIR_NATIVE}${base_libdir_native}"
STAGING_SBINDIR_NATIVE = "${STAGING_DIR_NATIVE}${sbindir_native}"
STAGING_INCDIR_NATIVE = "${STAGING_DIR_NATIVE}${includedir_native}"
STAGING_ETCDIR_NATIVE = "${STAGING_DIR_NATIVE}${sysconfdir_native}"
STAGING_DATADIR_NATIVE = "${STAGING_DIR_NATIVE}${datadir_native}"
####################################################################

#STAGING_DIR_HOST = "${STAGING_DIR}/${MACHINE}"
STAGING_BINDIR = "${STAGING_DIR_HOST}${bindir}"
STAGING_LIBDIR = "${STAGING_DIR_HOST}${libdir}"
STAGING_LIBEXECDIR = "${STAGING_DIR_HOST}${libexecdir}"
STAGING_BASELIBDIR = "${STAGING_DIR_HOST}${base_libdir}"
STAGING_INCDIR = "${STAGING_DIR_HOST}${includedir}"
STAGING_DATADIR = "${STAGING_DIR_HOST}${datadir}"
STAGING_EXECPREFIXDIR = "${STAGING_DIR_HOST}${exec_prefix}"
STAGING_LOADER_DIR = "${STAGING_DIR_HOST}/loader"
STAGING_FIRMWARE_DIR = "${STAGING_DIR_HOST}/firmware"
######################################################################

#STAGING_DIR_TARGET = "${STAGING_DIR}/${MACHINE}"
STAGING_DIR_TCBOOTSTRAP = "${STAGING_DIR_TARGET}-tcbootstrap"


### Path manipulations ###
prefix = "${STAGING_DIR_HOST}${prefix_native}"
base_prefix = "${STAGING_DIR_HOST}"
exec_prefix = "${STAGING_DIR_HOST}${prefix_native}"
bindir = "${exec_prefix}/bin"
sbindir = "${bindir}"
base_bindir = "${bindir}"
base_sbindir = "${bindir}"
libdir = "${exec_prefix}/lib"
libexecdir = "${exec_prefix}/libexec"
includedir = "${exec_prefix}/include"


EXTRA_OECONF_PATH=" \
	--prefix=${prefix} \ 
	--exec_prefix=${exec_prefix} \
	--bindir=${bindir} \
	--sbindir=${sbindir} \
	--libexecdir=${libexecdir} \
	--datadir=${datadir} \
	--sysconfdir=${sysconfdir} \
	--sharedstatedir=${sharedstatedir} \
	--localstatedir=${localstatedir} \
	--libdir=${libdir} \
	--includedir=${includedir} \
	--oldincludedir=${oldincludedir} \
	--infodir=${infodir} \
	--mandir=${mandir} \
"




EXTRA_OECONF = " \
	${EXTRA_OECONF_PATH} \
	--target=${MACHINE} \
"

inherit nopackages
