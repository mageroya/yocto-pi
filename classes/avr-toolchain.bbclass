DEPENDS = "libc-cross-avr"


# Set location to sysroot
STAGING_DIR = "${TMPDIR}/sysroots"
SYSROOT_DIR = "${STAGING_DIR}/avr"

# Set compiler and linker flags
export CFLAGS=" --sysroot=${SYSROOT_DIR} -Os -I${SYSROOT_DIR}/include"
export CXXFLAGS=" --sysroot=${SYSROOT_DIR} -Os -I${SYSROOT_DIR}/include"

export CC="avr-gcc "
export CPP="avr-gcc -E "
export CXX="avr-g++ "
export LD="avr-ld "
export AR="avr-ar "
export AS="avr-as "
export NM="avr-nm "
export RANLIB="avr-ranlib "
export OBJCOPY="avr-objcopy "
export OBJDUMP="avr-objdump "




#
# Example:
# export DEFINES=" -D__AVR_ATmega328P__ -DF_CPU=16000000UL"
#  ./avr-gcc -Os $DEFINES \
# --sysroot="<path_to_yocto_folder>/build-release/tmp/sysroots/avr" \
# -I<path_to_yocto_folder>/build-release/tmp/sysroots/avr/include \
# -mmcu=atmega328p \
# hello.c -o hello
#
