SUMMARY = "A basic console capable minimal image"
LICENSE = "MIT"


IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL} kernel-modules "

inherit core-image


DEV_TOOLS_INSTALL=" \
    binutils \
    binutils-symlinks \
    coreutils \
    cpp \
    cpp-symlinks \
    diffutils \
    file \
    g++ \
    g++-symlinks \
    gcc \
    gcc-symlinks \
    gdb \
    gdbserver \
    gettext \
    git \
    ldd \
    libstdc++ \
    libstdc++-dev \
    libtool \
    make \
    perl-modules \
    pkgconfig \
	strace \
	autoconf \ 
	automake \
	ccache \
"


BASE_UTILS_INSTALL=" \
    bzip2 \
	unzip \
	zip \
	cpio \
    fbset \
    findutils \
    i2c-tools \
    iperf \
    less \
    netcat \
    procps \
    rsync \
    sysfsutils \
    util-linux \
    wget \
	git \
	vim \
	python \
"


NET_UTILS_BASE=" \
	packagegroup-core-ssh-openssh \
	openssh-sftp-server \
	dhcp-client \
"


NET_UTILS_WIFI=" \
    crda \
    iw \
    linux-firmware-brcm43430 \
    linux-firmware-ralink \
    linux-firmware-rtl8192ce \
    linux-firmware-rtl8192cu \
    linux-firmware-rtl8192su \
    wireless-tools \
    wpa-supplicant \
"

NET_UTILS_WIFI_INSTALL="${@bb.utils.contains("MACHINE", "raspberrypi2", "${NET_UTILS_WIFI}", "" ,d)}"

NET_UTILS_ETH=" \
"


IMAGE_INSTALL_append=" \
	${BASE_UTILS_INSTALL} \
	${NET_UTILS_WIFI_INSTALL} \
	${NET_UTILS_BASE} \
"

IMAGE_INSTALL_append="${@bb.utils.contains("DISTRO_FEATURES", "developer-utils", "${DEV_TOOLS_INSTALL}", "" ,d)}"


python() {
	if bb.utils.contains("MACHINE", "raspberrypi2", "${NET_UTILS_WIFI}", "" ,d) == "":
		bb.warn('Image "{0}" should be build with machine raspberrypi2 but is build with machine "{1}!"'\
				.format(d.getVar('PN', True), d.getVar('MACHINE', True)))
}
IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"
export IMAGE_BASENAME = "sm-core-image"
