SUMMARY = "A basic qt capable minimal image"
LICENSE = "MIT"


require sm-core-image.bb


QT_DEMOS_INSTALL="${@bb.utils.contains("DISTRO_FEATURES", "qt-demos", "packagegroup-qt5-demos", "" ,d)}"


QT_BASE_INSTALL = " \
    packagegroup-qt5-toolchain-target \
"

IMAGE_INSTALL_append=" \
	${QT_BASE_INSTALL} \
	${QT_DEMOS_INSTALL} \
"
