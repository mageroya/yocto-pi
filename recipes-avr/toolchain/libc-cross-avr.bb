# Copyright © Benedikt Niedermayr 2016

# In order to get avr-libc properly compiled and in installed to an separate sysroot directory
# a deeper hack into yocto has to be done.
# Steps to achieve this:
# * Create new SYSROOT
# * Use avr-gcc for compiling instead of MACHINE specific selected one
# * Stage to new SYSROOT
#
#
# For now we won't create an extra sysroot 


STAGING_DIR_HOST = "${STAGING_DIR}/${MACHINE}"
inherit avr

SUMMARY = "glibc for avr cross compiler"
SECTION = "devel"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "\
    file://LICENSE;md5=8d91a8f153d3de715f67a5df0d841c43 \
"


BPN="avr"
S = "${WORKDIR}/avr-libc-2.0.0"
WORKDIR = "${BASE_WORKDIR}/${BPN}/${PN}/${EXTENDPE}${PV}-${PR}"
DEPENDS += " gcc-cross-avr "

SRC_URI =" http://download.savannah.gnu.org/releases/avr-libc/avr-libc-2.0.0.tar.bz2 "
SRC_URI[md5sum] = "2360981cd5d94e1d7a70dfc6983bdf15"
SRC_URI[sha256sum] = "b2dd7fd2eefd8d8646ef6a325f6f0665537e2f604ed02828ced748d49dc85b97"




### build opts and flags ###
SYSROOT_OPT=" --sysroot=${STAGING_DIR_NATIVE}"
export CC="avr-gcc ${SYSROOT_OPT} "
export CXX="avr-g++ ${SYSROOT_OPT} "
export CPP="avr-gcc -E ${SYSROOT_OPT} "
export LD="avr-ld ${SYSROOT_OPT} "
export AS="avr-as"
export AR="avr-ar"
export NM="avr-nm"

export BUILD_AS="avr-as" 

BASE_CFLAGS="-i${STAGING_DIR_NATIVE}/usr/include"
BASE_LDFLAGS="-L${STAGING_DIR_NATIVE}/usr/lib ${STAGING_DIR_NATIVE}/lib"
export CXXFLAGS="${BASE_CFLAGS} "
export CFLAGS="${BASE_CFLAGS} "
export LDFLAGS="${BASE_LDFLAGS} "



do_configure() {
	./configure ${EXTRA_OECONF} \
				--build="${MACHINE}" \
				--prefix="${prefix}"
}


do_compile() {
	oe_runmake -j8
}

#
# Somehow files for lib/* won't get installed to locations specified in --libdir
# They're getting installed to ${exec_prefix}/avr/* instead to ${exec_prefix}/*
# The second problem is the absolute path appended to ${D} (${D}+absolute_path).
# So cleanup the absolute path with do_install() and copy all files to required
# sysroot location with do_populate_sysroot()
#
do_install () {
	make 'DESTDIR=${D}' install
	TDIR=$(mktemp -d) 	
	mv ${D}${exec_prefix}/avr/* ${TDIR}
	mv ${D}${exec_prefix}/bin ${TDIR}
	mv ${D}${exec_prefix}/share ${TDIR}
	rm -r ${D}/*
	mv ${TDIR}/* ${D}
	rm -d ${TDIR}	
}

do_populate_sysroot() {
	cp ${D}${exec_prefix}/avr/* ${STAGING_DIR_HOST}
	cp ${D}${exec_prefix}/bin ${STAGING_DIR_HOST}
	cp ${D}${exec_prefix}/share ${STAGING_DIR_HOST}
	
}
