# Copyright © Benedikt Niedermayr 2016

ORIG_BINUTILS_DIR="${BSPDIR}/sources/poky/meta/recipes-devtools/binutils/"

FILESEXTRAPATHS_prepend := "${BSPDIR}/sources/poky/meta/recipes-devtools/binutils/binutils:"

# Set target
TARGET_PREFIX="avr-"
TARGET_ARCH="avr"


# Include common binutils settings
require ${ORIG_BINUTILS_DIR}/binutils.inc

# Extra configure options and installation of binutils as cross variant
require ${ORIG_BINUTILS_DIR}/binutils-cross.inc

# Downloading all sources 
require ${ORIG_BINUTILS_DIR}/binutils-${PV}.inc



