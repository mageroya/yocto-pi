# Copyright © Benedikt Niedermayr 2016

SUMMARY = "GNU cc and gcc C compilers"
HOMEPAGE = "http://www.gnu.org/software/gcc/"
SECTION = "devel"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "\
    file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552 \
"

PV  = "5.4.0" 
S = "${WORKDIR}/gcc-${PV}"
B = "${WORKDIR}/build"


# set all required paths (sysroot flags etc.) 
inherit avr 
inherit cross



DEPENDS=" mpfr gmp libmpc zlib binutils-cross-avr"
NATIVEDEPS = "mpfr-native gmp-native libmpc-native zlib-native"

PROVIDES = "virtual/${TARGET_PREFIX}gcc virtual/${TARGET_PREFIX}g++"

BASE_URI="${GNU_MIRROR}/gcc/gcc-${PV}/gcc-${PV}.tar.bz2"
SRC_URI=" \
	${BASE_URI} \
"

SRC_URI[md5sum] = "4c626ac2a83ef30dfb9260e6f59c2b30"
SRC_URI[sha256sum] = "608df76dec2d34de6558249d8af4cbee21eceddbcb580d666f7a5a583ca3303a"



EXTRA_OECONF_append=" \
	--target=avr \
	--enable-languages=c,c++ \
	--disable-nls \
	--disable-libssp \
	--with-dwarf2 \
	--with-mpfr=${STAGING_DIR_NATIVE}${prefix_native} \
	--with-system-zlib \
	--enable-long-long \
	--host="${HOST_ARCH}" \
	--build="${HOST_ARCH}" \
	--with-build-sysroot=${STAGING_DIR_NATIVE} \
"


do_configure() {
	../gcc-${PV}/configure ${EXTRA_OECONF}
	
}


do_compile() {
	oe_runmake -j8
}


do_install() {
	oe_runmake 'DESTDIR=${D}' install
}



#
# Copy gcc executables to libexec search path
#
SRCDIR="${libexecdir}/gcc/${MACHINE}/${PV}"
create_links() {
	cd ${bindir}
	for i in $(ls); do
		new=$(echo $i | sed 's/avr-//g')
		ln -sf ${bindir}/${i} ${SRCDIR}/${new}
	done
}

do_populate_sysroot[postfuncs] += "create_links "


